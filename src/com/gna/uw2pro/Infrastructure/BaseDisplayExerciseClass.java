package com.gna.uw2pro.Infrastructure;

import com.gna.uw2pro.R;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class BaseDisplayExerciseClass extends CommonBaseClass {
	protected String[] exerciseTextArray;
	protected String[] exerciseNameArray;
	protected int[] firstPictureArray;
	protected int[] secondPictureArray;
	protected int[] thirdPictureArray;
	protected int[] imageCount;

	protected ImageView firstImage;
	protected ImageView secondImage;
	protected ImageButton rightArrowBtn;
	protected ImageButton leftArrowBtn;
	protected ImageButton cancelBtn;
	protected TextView exerciseText;
	protected TextView exerciseCount;
	protected TextView exerciseName;
	protected ImageView titleView;
	protected ImageButton infoBtn;

	public void intializeViews() {
		firstImage = (ImageView) findViewById(R.id.firstImage);
		secondImage = (ImageView) findViewById(R.id.secondImage);
		rightArrowBtn = (ImageButton) findViewById(R.id.rightArrowBtn);
		leftArrowBtn = (ImageButton) findViewById(R.id.leftArrowBtn);
		exerciseText = (TextView) findViewById(R.id.exerciseText);
		exerciseCount = (TextView) findViewById(R.id.exerciseCount);
		exerciseName = (TextView) findViewById(R.id.exerciseName);
		titleView = (ImageView) findViewById(R.id.titleView);
		cancelBtn = (ImageButton) findViewById(R.id.cancelBtn);
		infoBtn = (ImageButton) findViewById(R.id.infoBtn);

	}

	public void workoutAExercise() {
		titleView.setBackgroundResource(R.drawable.title01);
		exerciseTextArray = new String[7];
		exerciseNameArray = new String[7];
		firstPictureArray = new int[7];
		secondPictureArray = new int[7];
		thirdPictureArray = new int[7];
		imageCount = new int[7];

		exerciseTextArray[0] = "In a squat position, squat down legs parallel, then explode up and jump, land softly and repeat.";
		exerciseTextArray[1] = "In a push up position, bring your left knee under and across your body. Hold. Repeat the other side.";
		exerciseTextArray[2] = "Holding two dumbbells at a 90 degree frontal position, tense your core tight and begin to squat down and then explode upward as you press the DBs upward.";
		exerciseTextArray[3] = "Bend at your wait with a flat back and knees bent slightly.Flye your DBs outward and back, pause return and repeat";
		exerciseTextArray[4] = "Holding two dumbbells in a push up position, brace your core so you do not dip or arch your hips, begin to row alternately each dumbbell to your side, pausing then return and repeat.";
		exerciseTextArray[5] = "Laying on your back, hold two dumbbells outward in a flye position, , then lift your feet together. holding the legs almost vertical as you begin to bring the weights together with straight arms. Repeat.";
		exerciseTextArray[6] = "Holding 1 dumbbell, and in a laying position with knees bent.  Crunch your abs as you raise the dumbbell upward, pause, return and repeat.";

		exerciseNameArray[0] = "BW Squat Jumps";
		exerciseNameArray[1] = "Knee Unders";
		exerciseNameArray[2] = "Front Thruster";
		exerciseNameArray[3] = "Bent Reverse DB Flye";
		exerciseNameArray[4] = "Renegade Row";
		exerciseNameArray[5] = "Sky Pointer Flyes";
		exerciseNameArray[6] = "DB Finger Point Crunch";

		firstPictureArray[0] = R.drawable.w1_exercise01_01;
		firstPictureArray[1] = R.drawable.w1_exercise02_01;
		firstPictureArray[2] = R.drawable.w1_exercise03_01;
		firstPictureArray[3] = R.drawable.w1_exercise04_01;
		firstPictureArray[4] = R.drawable.w1_exercise05_01;
		firstPictureArray[5] = R.drawable.w1_exercise06_01;
		firstPictureArray[6] = R.drawable.w1_exercise07_01;

		secondPictureArray[0] = R.drawable.w1_exercise01_02;
		secondPictureArray[1] = R.drawable.w1_exercise02_02;
		secondPictureArray[2] = R.drawable.w1_exercise03_02;
		secondPictureArray[3] = R.drawable.w1_exercise04_02;
		secondPictureArray[4] = R.drawable.w1_exercise05_02;
		secondPictureArray[5] = R.drawable.w1_exercise06_02;
		secondPictureArray[6] = R.drawable.w1_exercise07_02;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = 0;
		thirdPictureArray[4] = 0;
		thirdPictureArray[5] = 0;
		thirdPictureArray[6] = 0;

		imageCount[0] = 2;
		imageCount[1] = 2;
		imageCount[2] = 2;
		imageCount[3] = 2;
		imageCount[4] = 2;
		imageCount[5] = 2;
		imageCount[6] = 2;

	}

	public void workoutBExercise() {
		titleView.setBackgroundResource(R.drawable.title02);
		exerciseTextArray = new String[8];
		exerciseNameArray = new String[8];
		firstPictureArray = new int[8];
		secondPictureArray = new int[8];
		thirdPictureArray = new int[8];
		imageCount = new int[8];

		exerciseTextArray[0] = "At a lat pulldown machine, take up a wide grip.  Bring the bar down to your chest, pause and return up slower.  repeat (Do a pull up if you don't have access to this machine).";
		exerciseTextArray[1] = "With a bench set at 45 degrees, take two DBs and lay back, let weights hang then curl upward and lower under control.repeat.";
		exerciseTextArray[2] = "With hands close together under shoulders, in a push up position.  Begin the move, keeping elbows tight to your side.  Drop to knees if this is too hard.";
		exerciseTextArray[3] = "Get ready to do a lunge, make sure knee is at 90 degrees as you lunge diagonally and raise your arms high and vertical.  Repeat the other side.";
		exerciseTextArray[4] = "Drop to your hands, explode your feet back and out to a temporary push up position, then jump feet to your chest, and explode upward into a jump. Repeat.";
		exerciseTextArray[5] = "Perform a body weight squat, as you explode upward, you jump and twist in the air, landing facing the other way. Then do one rep and on next, jump back the other way.  Change from clockwise to anti-clockwise so you do not spin same direction each time.";
		exerciseTextArray[6] = "Holding a DB with underhand grip in left had, lean forward raising the DB as your left leg goes backwards, pause trying to get as flat as possible with core tensed.  Repeat other side.";
		exerciseTextArray[7] = "In a push up stance, tense your core. Bring your left leg up and outward, bending it.  Pause and then repeat the other side.  You can do an optional push up between each knee out.";

		exerciseNameArray[0] = "Wide Pulldown";
		exerciseNameArray[1] = "Incline 45 Curl";
		exerciseNameArray[2] = "CG Push Up";
		exerciseNameArray[3] = "Diagonal Alt Lunges";
		exerciseNameArray[4] = "Burpees";
		exerciseNameArray[5] = "180 Jump Squats";
		exerciseNameArray[6] = "DB Lean";
		exerciseNameArray[7] = "Knee Outs";

		firstPictureArray[0] = R.drawable.w2_exercise01_01;
		firstPictureArray[1] = R.drawable.w2_exercise02_01;
		firstPictureArray[2] = R.drawable.w2_exercise03_01;
		firstPictureArray[3] = R.drawable.w2_exercise04_01;
		firstPictureArray[4] = R.drawable.w2_exercise05_01;
		firstPictureArray[5] = R.drawable.w2_exercise06_01;
		firstPictureArray[6] = R.drawable.w2_exercise07_01;
		firstPictureArray[7] = R.drawable.w2_exercise08_01;

		secondPictureArray[0] = R.drawable.w2_exercise01_02;
		secondPictureArray[1] = R.drawable.w2_exercise02_02;
		secondPictureArray[2] = R.drawable.w2_exercise03_02;
		secondPictureArray[3] = R.drawable.w2_exercise04_02;
		secondPictureArray[4] = R.drawable.w2_exercise05_02;
		secondPictureArray[5] = R.drawable.w2_exercise06_02;
		secondPictureArray[6] = R.drawable.w2_exercise07_02;
		secondPictureArray[7] = R.drawable.w2_exercise08_02;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = 0;
		thirdPictureArray[4] = R.drawable.w2_exercise05_03;
		thirdPictureArray[5] = 0;
		thirdPictureArray[6] = 0;
		thirdPictureArray[7] = 0;

		imageCount[0] = 2;
		imageCount[1] = 2;
		imageCount[2] = 2;
		imageCount[3] = 2;
		imageCount[4] = 4;
		imageCount[5] = 2;
		imageCount[6] = 2;
		imageCount[7] = 2;

	}

	public void workoutCExercise() {
		titleView.setBackgroundResource(R.drawable.title03);
		exerciseTextArray = new String[7];
		exerciseNameArray = new String[7];
		firstPictureArray = new int[7];
		secondPictureArray = new int[7];
		thirdPictureArray = new int[7];
		imageCount = new int[7];

		exerciseTextArray[0] = "With palms flat perform the push up.Go on your knees if needed.";
		exerciseTextArray[1] = "Laying on your stomach, raise both your feet and your arms. Pause and the repeat.";
		exerciseTextArray[2] = "Grabbing an over head bar etc, perform a pull up, pause at the top then lower slowly.";
		exerciseTextArray[3] = "Drop to your hands,explode your feet back and out to a temporary push up position, at this point raise one knee out and up towards your ear, repeat the other side, then jump feet to your chest, and explode upward into a jump. Repeat.";
		exerciseTextArray[4] = "On your back, lift your legs to vertical.Arms at side or holding a stationary object for support over your head. Lift your hips, stopping your legs from swaying. Lower to start and repeat.";
		exerciseTextArray[5] = "From standing, bend and lean forward to place palms on the floor.'Walk' out on hands four paces, do a push up, 'walk' back ward to start position and repeat.";
		exerciseTextArray[6] = "Similar to sky pointers except this time have the soles of your feet together, knees split.  Keep this position as you then raise your legs, lift your hips, then return to start and repeat.";

		exerciseNameArray[0] = "Push Up";
		exerciseNameArray[1] = "Floor Superman";
		exerciseNameArray[2] = "Pull Up";
		exerciseNameArray[3] = "Burpee Knee Outs";
		exerciseNameArray[4] = "Sky Pointers";
		exerciseNameArray[5] = "Spiderman Walk outs";
		exerciseNameArray[6] = "Split Frogs";

		firstPictureArray[0] = R.drawable.w3_exercise01_01;
		firstPictureArray[1] = R.drawable.w3_exercise02_01;
		firstPictureArray[2] = R.drawable.w3_exercise03_01;
		firstPictureArray[3] = R.drawable.w3_exercise04_01;
		firstPictureArray[4] = R.drawable.w3_exercise05_01;
		firstPictureArray[5] = R.drawable.w3_exercise06_01;
		firstPictureArray[6] = R.drawable.w3_exercise07_01;

		secondPictureArray[0] = R.drawable.w3_exercise01_02;
		secondPictureArray[1] = R.drawable.w3_exercise02_02;
		secondPictureArray[2] = R.drawable.w3_exercise03_02;
		secondPictureArray[3] = R.drawable.w3_exercise04_02;
		secondPictureArray[4] = R.drawable.w3_exercise05_02;
		secondPictureArray[5] = R.drawable.w3_exercise06_02;
		secondPictureArray[6] = R.drawable.w3_exercise07_02;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = R.drawable.w3_exercise04_03;
		thirdPictureArray[4] = 0;
		thirdPictureArray[5] = 0;
		thirdPictureArray[6] = 0;

		imageCount[0] = 2;
		imageCount[1] = 2;
		imageCount[2] = 2;
		imageCount[3] = 4;
		imageCount[4] = 2;
		imageCount[5] = 2;
		imageCount[6] = 2;

	}

	public void workoutDExercise() {
		titleView.setBackgroundResource(R.drawable.title04);
		exerciseTextArray = new String[8];
		exerciseNameArray = new String[8];
		firstPictureArray = new int[8];
		secondPictureArray = new int[8];
		thirdPictureArray = new int[8];
		imageCount = new int[8];

		exerciseTextArray[0] = "Holding DBs at 90 degrees, perform a squat, but tense your core and do not move the weights.";
		exerciseTextArray[1] = "Holding 2 DBs, stand and begin raising only the left DB upward in a shoulder press.  Tense your core to stop body sway.  Then repeat all reps on other side.";
		exerciseTextArray[2] = "Holding DBs, begin to lunge backward, controlling the move so you do not wobble.  return and repeat the other side.";
		exerciseTextArray[3] = "In a push up position, swing your left leg under your body and 'outward' as your right arm moves vertical and you balance on your left hand and right foot, pause then return under control and repeat other side.";
		exerciseTextArray[4] = "On your back, with your hands locked down under heavy dumbbell grips or a bench, perform the sky pointer , don't let your legs swing but move in a vertical motion.";
		exerciseTextArray[5] = "Holding DBs high at 90 degree shoulder position, squat down then explode up from the squat as you pres the DBs overhead, return and repeat.";
		exerciseTextArray[6] = "Bend at the wait, bend your legs to relieve pressure on the lower back, lock elbows to your sides and kickback the weights moving only your forearms back and forth.";
		exerciseTextArray[7] = "In the plank position, hold the move completely static.";

		exerciseNameArray[0] = "Db High Squat";
		exerciseNameArray[1] = "Single Arm DB Press";
		exerciseNameArray[2] = "Rev DB Lunges";
		exerciseNameArray[3] = "Breakdancers";
		exerciseNameArray[4] = "Lockdown Sky Pointers";
		exerciseNameArray[5] = "Side DB Thrusters";
		exerciseNameArray[6] = "Kickbacks";
		exerciseNameArray[7] = "Plank";

		firstPictureArray[0] = R.drawable.w4_exercise01_01;
		firstPictureArray[1] = R.drawable.w4_exercise02_01;
		firstPictureArray[2] = R.drawable.w4_exercise03_01;
		firstPictureArray[3] = R.drawable.w4_exercise04_01;
		firstPictureArray[4] = R.drawable.w4_exercise05_01;
		firstPictureArray[5] = R.drawable.w4_exercise06_01;
		firstPictureArray[6] = R.drawable.w4_exercise07_01;
		firstPictureArray[7] = R.drawable.w4_exercise08_01;

		secondPictureArray[0] = R.drawable.w4_exercise01_02;
		secondPictureArray[1] = R.drawable.w4_exercise02_02;
		secondPictureArray[2] = R.drawable.w4_exercise03_02;
		secondPictureArray[3] = R.drawable.w4_exercise04_02;
		secondPictureArray[4] = R.drawable.w4_exercise05_02;
		secondPictureArray[5] = R.drawable.w4_exercise06_02;
		secondPictureArray[6] = R.drawable.w4_exercise07_02;
		secondPictureArray[7] = 0;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = R.drawable.w4_exercise04_03;
		thirdPictureArray[4] = 0;
		thirdPictureArray[5] = 0;
		thirdPictureArray[6] = 0;
		thirdPictureArray[7] = 0;

		imageCount[0] = 2;
		imageCount[1] = 2;
		imageCount[2] = 2;
		imageCount[3] = 3;
		imageCount[4] = 2;
		imageCount[5] = 2;
		imageCount[6] = 2;
		imageCount[7] = 1;

	}

	public void workoutEExercise() {
		titleView.setBackgroundResource(R.drawable.title05);
		exerciseTextArray = new String[8];
		exerciseNameArray = new String[8];
		firstPictureArray = new int[8];
		secondPictureArray = new int[8];
		thirdPictureArray = new int[8];
		imageCount = new int[8];

		exerciseTextArray[0] = "Laying on your back, with DBS in a close grip on your chest, press them upward and back in a controlled speed.";
		exerciseTextArray[1] = "As you lunge forward, keep your back straight and curl 2 DBS upward, pause then return and repeat with other leg.";
		exerciseTextArray[2] = "Hold a DB, turn and squat down, placing the DB at your inside ankle, then twist and squat upward to the other side diagonally and 'put' the DB on an imaginary shelf.Pause then return ad repeat.Then do all reps on other side.";
		exerciseTextArray[3] = "Perform a jumping jack as you press DBS at the same time.";
		exerciseTextArray[4] = "Curl 2 DBS upward but have a hammer grip so the palms are facing inward.";
		exerciseTextArray[5] = "This requires you place one hand close to your side, the other out in front of you,  then perform a push up.  Next circuit you will switch hands.";
		exerciseTextArray[6] = "A fast explosive move: squat down, legs parallel, then explode upward reaching as high as possible, then as you come down land softly and repeat immediately aiming for continual motion.";
		exerciseTextArray[7] = "On your back, lift your foot upward and raise from the waist lifting shoulders off the floor your butt as the pivot. Touch left foot with left hand, return and repeat on other side.";

		exerciseNameArray[0] = "CG DB Floor Press";
		exerciseNameArray[1] = "Lunge & Curl";
		exerciseNameArray[2] = "DB Put Ups";
		exerciseNameArray[3] = "DB Jumping Jacks";
		exerciseNameArray[4] = "Hammer Curls";
		exerciseNameArray[5] = "Zig Zag Push Up";
		exerciseNameArray[6] = "Total Body Power";
		exerciseNameArray[7] = "Half & Half";

		firstPictureArray[0] = R.drawable.w5_exercise01_01;
		firstPictureArray[1] = R.drawable.w5_exercise02_01;
		firstPictureArray[2] = R.drawable.w5_exercise03_01;
		firstPictureArray[3] = R.drawable.w5_exercise04_01;
		firstPictureArray[4] = R.drawable.w5_exercise05_01;
		firstPictureArray[5] = R.drawable.w5_exercise06_01;
		firstPictureArray[6] = R.drawable.w5_exercise07_01;
		firstPictureArray[7] = R.drawable.w5_exercise08_01;

		secondPictureArray[0] = R.drawable.w5_exercise01_02;
		secondPictureArray[1] = R.drawable.w5_exercise02_02;
		secondPictureArray[2] = R.drawable.w5_exercise03_02;
		secondPictureArray[3] = R.drawable.w5_exercise04_02;
		secondPictureArray[4] = R.drawable.w5_exercise05_02;
		secondPictureArray[5] = R.drawable.w5_exercise06_02;
		secondPictureArray[6] = R.drawable.w5_exercise07_02;
		secondPictureArray[7] = R.drawable.w5_exercise08_02;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = 0;
		thirdPictureArray[4] = 0;
		thirdPictureArray[5] = 0;
		thirdPictureArray[6] = 0;
		thirdPictureArray[7] = 0;

		imageCount[0] = 2;
		imageCount[1] = 2;
		imageCount[2] = 2;
		imageCount[3] = 2;
		imageCount[4] = 2;
		imageCount[5] = 2;
		imageCount[6] = 2;
		imageCount[7] = 2;

	}

}
